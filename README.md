# fleet keycloak operator demo

This example will deploy the [Keycloak Operator](https://www.keycloak.org/operator/installation#_installing_by_using_kubectl_without_operator_lifecycle_manager) application
using Rancher [fleet](https://fleet.rancher.io) and kustomize.

The operator will be deployed into the `keycloak-system` namespace on the Rancher Downstream Cluster.

```yaml
---
# task: gitrepo for keycloak operator

apiVersion: fleet.cattle.io/v1alpha1
kind: GitRepo
metadata:
  name: keycloak
  namespace: fleet-default
spec:
  branch: demo

  correctDrift:
    enabled: true

  paths:
    - operator

  repo: https://gitlab.com/tschb1/fleet-keycloak

  targets:
    - name: dev
      clusterSelector:
        matchLabels:
          tier: dev

    - name: test
      clusterSelector:
        matchLabels:
          tier: test

    - name: prod
      clusterSelector:
        matchLabels:
          tier: prod
```

The keycloak instance `example`:

```yaml
---
# task: fleet gitrepo for keycloak instance example

apiVersion: fleet.cattle.io/v1alpha1
kind: GitRepo
metadata:
  name: keycloak-example
  namespace: fleet-default
spec:
  branch: demo

  correctDrift:
    enabled: true

  paths:
    - demo

  repo: https://gitlab.com/tschb1/fleet-keycloak

  targets:
    - name: dev
      clusterSelector:
        matchLabels:
          tier: dev

    - name: test
      clusterSelector:
        matchLabels:
          tier: test

    - name: prod
      clusterSelector:
        matchLabels:
          tier: prod
```
